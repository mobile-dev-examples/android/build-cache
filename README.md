# Speed up build times with caching
When creating a pipeline, one main focus is ensuring jobs are running as efficient and quick as possible. To solve this, cache directories
can be mounted to the pipeline for dependency reuse for subsequent runs.. For Android, targeting the following directories`~/.gradle/caches` 
and `~/.gradle/wrapper` for caching will speed up build times.

## Requirements
GitLab CE or Above

## Usage

Export environment variable `GRADLE_USER_HOME` which contains gradle cache. Mark gradlew as executable.
```before_script:
  - export GRADLE_USER_HOME=$(pwd)/.gradle
  - chmod +x ./gradlew
```

Utiilize [cache key](https://docs.gitlab.com/ee/ci/yaml/#cachekey) to define how the cache will be handled within the pipeline or jobs. 
Then set which array of directory [paths](https://docs.gitlab.com/ee/ci/yaml/#cachepaths) you want cached for reuse.
```cache:
  key: ${CI_PROJECT_ID} #stay same between jobs
  paths:
     - .gradle/wrapper
     - .gradle/caches
```

# Result

The first build will not recognize a cache, as shown below.

![Before](cache_not_saved.png "Before cache")


The cache in the following build will be recalled saving time on downloading any future gradle artifacts.

![Before](cache_saved.png "Cached extracted after initial run")

# Resources
https://docs.gitlab.com/ee/ci/caching/

https://blog.gradle.org/introducing-gradle-build-cache

https://guides.gradle.org/using-build-cache/#build_cache_performance


## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
For issue tracking, please use this [project](https://gitlab.com/mobile-dev-examples/android/issue-tracking).


